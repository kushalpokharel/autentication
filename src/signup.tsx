import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import {Link} from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import {useHistory} from 'react-router-dom';



const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function SignUp() {
  const classes = useStyles();
  const history = useHistory();

  const[values,setValues] = React.useState({
    email:"",
    password:"",
    confirmpassword:""
  });

  const [error,setError] = React.useState({email:"",password:""});

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) =>{
    setValues({...values,[e.target.name]:e.target.value})
  }

  function validate()
  {
    
    const email = values.email;
    const regex = /.+@.+\..+/
    let temp = {email:"",password:""}
    if(!regex.test(email)){
      temp.email = "Email is invalid";
      setError({...temp});
      return 0;
    }
    const password = values.password;
    const confirm = values.confirmpassword;
    console.log((/[0-9]/).test(password))
    if(!(/[0-9]/).test(password)||!/[a-z]/.test(password))
    {
      temp.password = "Password doesn't contain at least a number or alphabet ";
      setError({...temp});
      
      return 0;
    }
    else if(password!==confirm)
    { 
      console.log(password);
      console.log(confirm);
      temp.password = "Passwords don't match";
      setError({...temp});
      
      return 0;
    }
    setError({...temp});
    return 1;
  }

  const saveData = ()=>{
    localStorage.setItem("email",values.email);
    localStorage.setItem("password",values.password);
    setValues({email:"",
    password:"",
    confirmpassword:""});

    history.push("/signin");
  }

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) =>{
    e.preventDefault();
    const a = validate();
    if(a)
    {
     
       saveData();
        alert("Registration successfull");
    }
    else 
      return;
    
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          {/* <LockOutlinedIcon /> */}
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <form className={classes.form} onSubmit={handleSubmit}>
          <Grid container spacing={2}>
          
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                type="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                value = {values.email}
                onChange = {handleChange}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                error={error.password===""?false:true}
                helperText = {error.password}
                name="password"
                label="Password"
                type="password"
                value = {values.password}
                id="password"
                autoComplete="current-password"
                onChange = {handleChange}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="confirmpassword"
                value = {values.confirmpassword}
                label="Confirm Password"
                type="password"
                id="confirmpassword"
                
                onChange = {handleChange}
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign Up
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link to="/signin">
                Already have an account? Sign in
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      
    </Container>
  );
}


