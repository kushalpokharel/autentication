import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import SignUp from './signup';
import SignIn from './Signin';
import Dashboard from './dashboard'
import ProtectedRoute from "./ProtectedRoute";
import Unauthorized from './Unauthorized.jsx';

export default function App() {
  return (
    <Router>
     

        <Switch>
          <Route path="/signup" component={SignUp}>
            
          </Route>
          <Route path="/signin" component={SignIn}>
           
          </Route>
          <ProtectedRoute path="/dashboard" component={Dashboard}>
           
          </ProtectedRoute>
          <Route path="/unauthorized" component={Unauthorized}>
           
          </Route>
          <Redirect to="/signup"/>
            
          
        </Switch>

    </Router>
        
  );
}

