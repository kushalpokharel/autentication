import React from 'react';
import {Route,Redirect} from 'react-router-dom';

const ProtectedRoute = ({component:Component,...rest})=>{
    return(
        <Route {...rest} render={
            
                props =>{
                    let isauth = sessionStorage.getItem("isAuth");
                    console.log(isauth);
                    if(isauth){
                        console.log('here');
                        return <Component {...rest} {...props}/>
                        
                    }
                    else {
                        console.log("nothere");
                        return <Redirect to={
                            {
                                pathname:'/unauthorized',
                                state:{
                                    from:props.location
                                }
                            }
                        }/>
                    }
                    }
                }/>
    )
}

export default ProtectedRoute;